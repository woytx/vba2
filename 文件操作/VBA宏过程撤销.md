![输入图片说明](https://images.gitee.com/uploads/images/2021/0527/152335_f47c93b3_9179267.gif "141838_9761b791_9179267.gif")

```vb
'test_OnUndo1：保存单元格的值
'Rng：保存单元格对象
Dim test_OnUndo1, Rng As Range

'执行宏过程
Sub test()
    '将要被修改单元格的值和对象保存在公共变量里
    test_OnUndo1 = Range("A1:I10").Value
    Set Rng = Range("A1:I10")
    
    '对单元格计算或赋值的一些操作-----------
    Dim rn As Range, i As Integer
    For Each rn In Range("A1:I10")
        i = i + 1
        rn.Value = i
    Next
    '-------------------------------------
    
    '设置撤销过程
    Application.OnUndo "test", "test_OnUndo"
End Sub

'撤销过程
Private Sub test_OnUndo()
    Rng.Value = test_OnUndo1
End Sub
```
