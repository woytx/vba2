> 导出当前工作表到PDF文件。

```vb
Sub SaveToPDF()
    Dim filePath As String
    filePath = Range("FilePath").Value//在Excel定义名称，存放目标路径的信息
    ActiveSheet.ExportAsFixedFormat Type:=xlTypePDF, Filename:=filePath & "\" & ActiveSheet.Name _
        , Quality:=xlQualityStandard, IncludeDocProperties:=True, IgnorePrintAreas:=False, OpenAfterPublish:=False
    MsgBox "Finished!"
End Sub
```

