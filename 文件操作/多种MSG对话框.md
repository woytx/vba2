Excel自带对话框~~~~~~~~~~~~

![Excel自带对话框](https://images.gitee.com/uploads/images/2021/0527/151659_d149f71e_9179267.png "屏幕截图.png")
```vb
Sub test()
    MsgBox "对话框"
End Sub
```
API对话框~~~~~~~~~~~~~~~~~

![API对话框](https://images.gitee.com/uploads/images/2021/0527/145915_0a7708d3_9179267.png "屏幕截图.png")

```vb
Private Declare Function MessageBox Lib "user32" Alias "MessageBoxA" (ByVal hwnd As Long, ByVal lpText As String, _
                                                    ByVal lpCaption As String, ByVal wType As Long) As Long

Public Sub test()
    MessageBox 0, "提示", "标题", &H1000&
End Sub
```
API自动关闭对话框~~~~~~~~~~

![自动关闭对话框](https://images.gitee.com/uploads/images/2021/0527/150343_90bfc903_9179267.gif "dupsn-5deuc.gif")

```vb
Private Declare Function MsgBoxEx Lib "user32" Alias "MessageBoxTimeoutA" (ByVal hwnd As Long, ByVal lpText As String, _
        ByVal lpCaption As String, ByVal wType As VbMsgBoxStyle, ByVal wlange As Long, ByVal dwTimeout As Long) As Long
Private Sub a()
   MsgBoxEx 0, "请选择", "2秒后自动关闭", vbYesNo + vbInformation, 1, 2000
End Sub
```
Shell对话框~~~~~~~~~~~~~~

![Shell对话框](https://images.gitee.com/uploads/images/2021/0527/150459_9c1a2034_9179267.png "屏幕截图.png")

```vb
Sub WshShell()
    Dim WshShell
    Set WshShell = CreateObject("Wscript.Shell")
    WshShell.popup "执行完毕!", 1, "提示", 64
End Sub
```
利用菜单制作提示对话框~~~~

![利用菜单制作提示对话框](https://images.gitee.com/uploads/images/2021/0527/151417_eb6e3fdc_9179267.png "屏幕截图.png")

```vb
Sub test()
    '菜单提示（提示内容,提示标题，图标，x位置，y位置）
    Call MsgBoxa("提示已完成", "标题")
End Sub


Sub MsgBoxa(myMessage, Optional myTitle As String, Optional myFaceid As Long, Optional x1 As Long, Optional y1 As Long)
    On Error Resume Next
    CommandBars("tmpContextMenu").Delete
    On Error GoTo 0
    Dim m As CommandBar, i%
    Set m = CommandBars.Add("tmpContextMenu", msoBarPopup)
    If x1 = 0 Then x1 = 400
    If y1 = 0 Then y1 = 400
    Set regex1 = CreateObject("VBSCRIPT.REGEXP")
    With regex1
        .Global = True
        .Pattern = ".+"
    End With
    With m
        With .Controls.Add(msoControlButton, , , , True)
            If myTitle = "" Then .Caption = "★★★★★" Else .Caption = myTitle
            .FaceId = 1954
        End With
        Set c = regex1.Execute(myMessage)
        If c.Count > 0 Then
            For i = 0 To c.Count - 1
                With .Controls.Add(msoControlButton, , , , True)
                    .Caption = c.Item(i).Value
                    If i = 0 Then .FaceId = myFaceid: .BeginGroup = True
                End With
            Next i
        End If
        .ShowPopup x1, y1
    End With
End Sub
```

