
```vb
Function Dlookup(rg As Range, rgs As Range, tj As Range, Optional st As String = ",") As String
    '=Dlookup(查找内容，待返回查找内容，查询条件,链接符号[当n=0时有效])
    '语法说明:
    '查找内容：单元格区域
    '待返回查找内容: 单元格区域，与查找内容相同
    '将所有查找结果默认逗号连接，可更改其他符号
    Dim d As Object, arr, brr, cc As String, s As String, i As Integer
    arr = rg.Value
    brr = rgs.Value
    cc = tj
    Set d = CreateObject("scripting.dictionary")
    For i = 1 To UBound(arr)
        If arr(i, 1) = cc Then
            s = cc
            If Not d.exists(s) Then
                d(s) = brr(i, 1)
            Else
                d(s) = d(s) & st & brr(i, 1)
            End If
        End If
    Next
    Dlookup = d(s)
    Set d = Nothing
End Function
```
