```vb
'=Mlookup(查找内容tj，查找区域rgs,返回值所在的列数L,第M个,连接字符串，默认为“,”)
'1、查找内容：除了单个值外，还可以选取多个单元格，进行多条件查找。
'2、查找区域: 同VLOOKUP
'3、返回值的在列数L: 同VLOOKUP
'4、第M个：值为1就返回第1个符合条件的，值为2就返回第2个符合条件的....当值为-1值时，返回最后1个符合条件的值，值为0时返回所有查找结果并用逗号连接

Function Mlookup(tj As Range, rgs As Range, L As Integer, m As Integer, Optional str As String = ",") As String
    Dim arr1, ARR2, Ls
    Dim R, k, i As Integer, s As String, sr As String
    arr1 = tj.Value
    ARR2 = rgs.Value
    If VBA.IsArray(arr1) Then
        For Each R In arr1
            If R <> "" Then
                s = s & R
                Ls = Ls + 1
            End If
        Next R
    Else
        s = arr1
    End If
    If m > 0 Then '非查找最后一个
        For i = 1 To UBound(ARR2)
            sr = ""
            If Ls > 1 Then
                For q = 1 To Ls
                    sr = sr & ARR2(i, q)
                Next q
            Else
                sr = ARR2(i, 1)
            End If
            If sr = s Then
                k = k + 1
                If k = m Then
                    Mlookup = ARR2(i, L)
                    Exit Function
                End If
            End If
        Next i
    ElseIf m = 0 Then '查找所有值
        For i = 1 To UBound(ARR2)
            sr = ""
            If Ls > 1 Then
                For q = 1 To Ls
                    sr = sr & ARR2(i, q)
                Next q
            Else
                sr = ARR2(i, 1)
            End If
            If sr = s Then
                Mlookup = Mlookup & str & ARR2(i, L)
            End If
        Next i
        Mlookup = Right(Mlookup, Len(Mlookup) - Len(str))
        Exit Function
    Else '查找最后一个
        For i = UBound(ARR2) To 1 Step -1
            sr = ""
            If Ls > 1 Then
                For q = 1 To Ls
                    sr = sr & ARR2(i, q)
                Next q
            Else
                sr = ARR2(i, 1)
            End If
            If sr = s Then
                Mlookup = ARR2(i, L)
                Exit Function
            End If
        Next i
    End If
    Mlookup = ""
End Function
```